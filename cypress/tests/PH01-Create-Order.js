/// <reference types="cypress" />
const { home,customerSearch } = require('../pageObjects/homePage.json')
const { searchSKU, createPage } = require('../pageObjects/orderCreatePage.json')
const orderPage = require('../pageObjects/ordersPage.json')

let orderSeqNumber = '2010000887';

context('Phoenix Web automation', () => {
  beforeEach(() => {
    cy.login('admin2', 'telio123')
    cy.changeLanguage()
  })

  it('Login to application', () => {

    cy.get(home.orderCreateButton, { timeout: 7000 }).should('be.visible')
    cy.get(home.orderCreateButton).click()
    cy.get(customerSearch.searchCustomerBox, { timeout: 7000 }).type('Dharma {enter}')
    cy.wait(3000)
    cy.get(customerSearch.customerResultRow, { timeout: 5000 }).click()
    cy.get(createPage.addItemButton).click()
    cy.get(searchSKU.skuSearchBox).should('be.visible').type('coca {enter}')
    cy.get(searchSKU.firstSkuQtyInput, { timeout: 5000 }).clear().type('100', { force: true })
    cy.get(searchSKU.addToCartButton).click()
    cy.get(createPage.submitOrderButton).click()
    cy.get(createPage.OrderSuccessToastMessage).contains('Order created successfully')
    cy.get(createPage.OrderSuccessToastMessage).should(($msg) => {
      const text = $msg.text()
      let orderNumber = new Array();
      orderNumber = text.split(':')
      console.log('Order Created Success :: ', orderNumber[1])
      orderSeqNumber = orderNumber[1].replace("#", "")
      console.log(orderSeqNumber)
    })

  })

  it('Search Order', () => {
    cy.get(orderPage.searchBar).should('be.visible').type(orderSeqNumber).type('{enter}')
    cy.get(orderPage.searchResultRow, { timeout: 7000 }).should('be.visible')
    cy.get(orderPage.searchResultRow).contains(orderSeqNumber)
  })

})
