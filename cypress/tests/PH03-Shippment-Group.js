/// <reference types="cypress" />
const { home,customerSearch } = require('../pageObjects/homePage.json')
const { searchSKU, createPage } = require('../pageObjects/orderCreatePage.json')
const orderPage = require('../pageObjects/ordersPage.json')

let orderSeqNumber = '2010000922';

context('WMS tests', () => {
  beforeEach(() => {
    cy.clearCookies()
  })

  it('Login to WMS and create shipment group', () => {
    cy.visit('https://wms-stg.telio.me/')
    cy.get('[name="username"]').type('admintech@telio.vn')
    cy.get('[name="password"]').type('Telio123')
    cy.get('button[type="submit"]').click()
    cy.wait(3000)
    cy.visit('https://wms-stg.telio.me/#/shipment-group/create')
    cy.get('#SearchBar').type('2010000922 {enter}')
    // cy.get('[role="checkbox"]>td:nth-child(8)').click()
    // cy.get('[role="document"]>li:nth-child(32)').click()
    // cy.get('#action_button').click()
  })

})
