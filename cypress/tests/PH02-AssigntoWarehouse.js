/// <reference types="cypress" />
const { home,customerSearch } = require('../pageObjects/homePage.json')
const { searchSKU, createPage } = require('../pageObjects/orderCreatePage.json')
const orderPage = require('../pageObjects/ordersPage.json')

let orderSeqNumber = '2010000887';

context('OMS assign to warehouse test', () => {
  // afterEach(() => {
  //   cy.clearLocalStorage()
  // })

  it('Login to OMS and assign to warehouse', () => {
    cy.visit('https://oms-stg.telio.me/')
    cy.get('[name="username"]').type('admintech@telio.vn')
    cy.get('[name="password"]').type('Telio123')
    cy.get('button[type="submit"]').click()
    cy.get('#navigation-list+button').click()
    cy.get('[role="document"]>li:nth-child(3)').click()
    cy.visit('https://oms-stg.telio.me/#/sale-order/unassigned')
    cy.get('#SearchBar').type('2000412261 {enter}')
    cy.get('[role="checkbox"]>td:nth-child(8)').click()
    cy.get('[role="document"]>li:nth-child(32)').click()
    cy.get('#action_button').click()
  })

})
