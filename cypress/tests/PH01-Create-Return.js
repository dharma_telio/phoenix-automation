/// <reference types="cypress" />

let orderSeqNumber=2000409953;
context('Phoenix Web automation', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.get('#username', { timeout: 7000 })
      .type('admin2', { force: true }).should('have.value', 'admin2')
    cy.get('#password')

      .type('telio123', { force: true })
      .should('have.value', 'telio123')
    cy.get('#kc-login').click()
    cy.get('span.nav-MuiButton-label',{ timeout: 7000 }).should('be.visible').click()
    cy.get('div.nav-MuiPopover-paper button.nav-MuiButtonBase-root').click()
  })

  it('Create return for completed order', () => {
    
    cy.get('#filterButton', { timeout: 7000 }).should('be.visible')
    cy.get('#filterButton').click()
    cy.get('div.MuiCollapse-container.MuiCollapse-entered>div>div>div>div:nth-child(1)>div:nth-child(1)').click()
    // cy.get('#div.MuiPopover-paper>div', { timeout: 3000 }).scrollTo('bottom')
    cy.get('div.MuiPopover-paper>div>li:nth-child(7)', { timeout: 7000 }).click()
    .type('{esc}')
    cy.get('#applyButton').click()
    cy.wait(3000)
    cy.get('thead+tbody>tr:nth-child(1)>td:nth-child(2)', { timeout: 5000 }).click()
    cy.get('[aria-label="menu"]+ div.MuiGrid-root button.MuiButtonBase-root:nth-child(2)').click()
    cy.get('div.MuiPopover-paper>div>li:nth-child(1)').should('be.visible').click()
    cy.get('tbody>tr:nth-child(1) input[type="number"]').click().clear().type('10')
    cy.get('div.MuiSelect-selectMenu').click()
    // cy.wait(200)
    cy.get('ul.MuiMenu-list>li:nth-child(3)').click()
    cy.get('button.MuiButton-containedPrimary').click()
    cy.get('div.MuiToolbar-regular button.MuiButton-containedPrimary').click()
    cy.get('div.MuiSnackbar-anchorOriginBottomCenter>div>p').contains('Tạo đơn hàng thành công')
    // cy.get('h6>a').should(($msg) => {
    //   const text = $msg.text()
    //   let orderNumber=new Array();
    //   orderNumber=text.split(':')
    //   console.log('Order Created SUccess :: ',orderNumber[1])
    //   orderSeqNumber=orderNumber[1].replace("#", "")
    // })
    
  })

  // it('Search Order', () => {
  //   cy.get('#SearchBar').should('be.visible').type(orderSeqNumber)
  //   cy.get('#closeButton+div>button', { timeout: 7000 }).click()
  //   cy.wait(4000)
  //   cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(2)', { timeout: 7000 }).should('be.visible')
  //   cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(2)').contains(orderSeqNumber)
  
  //   // cy.get('.MuiButton-startIcon > .MuiSvgIcon-root').click()
  //   // cy.wait(3000)
  //   // cy.wait(20000)
  //   // cy.get('#SearchBar').should('be.visible').type('HN')
  //   // cy.get('#closeButton').click()
    
  //   // cy.get('tbody>tr:nth-child(1)>td:nth-child(5) input').type('100', { force: true })

  // })

})
