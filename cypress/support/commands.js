const { login } = require('../pageObjects/loginPage.json')
const { footer } = require('../pageObjects/homePage.json')

Cypress.Commands.add("login", (username, password) => {
    cy.visit('/')
    cy.get(login.usernameInput, { timeout: 7000 })
        .type(username, { force: true }).should('have.value', username)
    cy.get(login.passwordInput)
        .type(password, { force: true })
        .should('have.value', password)
    //Click on Login button
    cy.get(login.loginButton).click()
})

Cypress.Commands.add("changeLanguage", () => {
    cy.get(footer.selectLanguage, { timeout: 7000 }).click()
    cy.get(footer.EnglishLanguageText).click()
})